class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
      @first_name = first_name
      @last_name = last_name
      @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    @courses.each do |crse|
      if crse.conflicts_with?(course)
        raise "Cannot enroll in #{course}. Conflicts with #{crse}"
      end
    end

    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    course_credits = Hash.new(0)
    @courses.each do |course|
      course_credits[course.department] += course.credits
    end

    course_credits
  end
end
